# Bucles
======

## while
Hará la acción siempre y cuando sea **TRUE** o se de la condición.


## For - While 

Un bucle que se ejecuta siempre y cuando se mantenga una condición. 

````js
for (let i=1;i<20;i++){}
    while (a>5){}

````

## Do - while

Ejecuta las acciones antes de hacer las comprobaciones, al menos una vez. 

````js
do {}
while {}

````

````js
let a = 5
do {
    a++
    console.log(a)
}
while {a<5}
````
En el caso de arriba. **a** empieza siendo 5, hace un do, transformando a=6.  

````js
[...]]
while {a > 5}
````

En este caso, como **a** va a ser mayor que 5, no haría nada. 

````js
let a = 4
do {
    a++
    console.log(a)
}
while {a<5}
````

En este caso, empezaría siendo **a** = 4, haría un sumatorio, y luego no seguiría ya que a = 5. 


**Ejemplos de bucles**
```js

    /* do(i++)
        while(comprobarPares())numeros[a]
        */

        /*
        console.log(a.lenght)
            do {console.log(i)
            }
                while (i%2==0)
                alert(i);
        } */
    
        /*
         a = [2,4,6,8,10,12,14,15,16,18,20]
        for (i=0;i<numeros.lenght;i++){
            console.log(numeros[i])
        }
        */

        /*
        i=0;  
        while(numeros[i]%2==0){
            console.log(numeros[i]);
            i++}
        */

        /*
        for(i=0;i<numeros.lenght==0;i++){console.log(numeros[i])}; if (numeros[i]%2!=0) {break}}  */
         
        /* for(i=0;i<a.lenght==0;i++){
            console.log(a[i]); 
            if (a[i]%2!=0) {
                break
                }
            }  */
 ```


