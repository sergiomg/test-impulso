# Eventos
==============

## onload
Cuando se cargue por completo realiza una función. 
````js
window.onload=function(){}
````

## onclick 
Cuando se haga click en el elemento que está escuchando realiza función.

````js
document.getElementByID("").onlick=function(){}
````

## addEventListener
Permite adherirse a un objeto y realizar una función.

````js
variable.addEventListener("click",function(){})
````