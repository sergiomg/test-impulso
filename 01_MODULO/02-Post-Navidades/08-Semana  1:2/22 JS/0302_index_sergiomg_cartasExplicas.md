# EJERCICIO CARTAS

=======

## CÓDIGO JAVASCRIPT

Tienes una tarea, en base a esa tarea lo descompones en tareas más pequeñas.
Para hacer las cartas, tenemos que ir pensado:
¿Qué necesito primero?¿Cuántas cartas se necesitas?

A apartir de ahí, llegamos a la conclusión, que un sitio donde guardar cosas puede ser un array.

```js
baraja = new Array(4);
baraja[0] = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12];
baraja[1] = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12];
baraja[2] = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12];
baraja[3] = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12];

// o se podría
baraja = new Array(4);
baraja[0] = crearArray();
baraja[1] = crearArray();
baraja[2] = crearArray();
baraja[3] = crearArray();

function crearArray() {
  return [1, 2, 3, 4, 5, 6, 7, 10, 11, 12];
}

// Se quitan el 8 y el 9 por el tipo de juego de carta que estamos haciendo
```

Ahora solo tenemos las cartas, pero no sabemos su palo, y entonces nos planteamos crear un array con los palos. Para que cuando leamos ambos arrays creados, podramos conectarlos

```js
palos = ["oros", "espadas", "bastos", "copas"];
```

**¿Qué tenemos hasta ahora?**

Cuatro pilas con diez columnas. (4 palos y 10 cartas en cada palo)

**¿Siguiente paso?**

Necesitamos sacar una carta aleatoria.
Para ello vamos a crear una función que me de una carta:

Primero hacemos uno para coger una carta del 0 al 3 (4 palos) y además tenemos que quitarle los decimales si usamos Math.
Y luego otro que nos de un número del 0 al 9 (10 cartas).
Esos resultados, se lo asignamos a una variable llamada resultado, que es básicamente "baraja[leches][cartas]"

Ahora llegaríamos a un problema:

**¿Qué podemos hacer para evitar que se repita una carta?**

Hay varias soluciones, una de ellas es guardar en un array todas aquellas cartas que ya han salido, y si sale esa carta repetida, vuelvo a ejecutarlo.

En este caso, es más útil, darle un valor a esa combinación. Para eso hacemos un bucle while que si sale un palo y carta, le asignamos la palabra "usada". Cuando sale una con esta característica, sigue lanzando la función hasta que salga una que no tenga "usada"

```js
function mostrarCarta() {
  do {
    leches = Math.floor(Math.random() * 4);
    cartas = Math.floor(Math.random() * 10);
    resultado = baraja[leches][cartas];
  } while (
    // problema de cartas usadas
    resultado == "usada"
  );

  // hasta aquí el bucle
  //Se le añade esto al resultado
  baraja[leches][cartas] = "usada";
  return resultado + " de " + palos[leches];
}
```

### Problema 1:

Queremos mostrar la carta, para ello, vamos a crear un nuevo elemento usando lo que ha devuelto la anterior función, para ello, usamos los métodos createElement y le damos el valor de mostrarCarta inyectandolo en su html (innerHTML).

Ahora, le añadimos a ese nuevo elemento que sea el hijo del div con el ID en el HTML "carta".

### Problema 2:

Necesitamos saber cuántas veces hay que pintar las cartas, como sabemos que la baraja tiene 40 cartas, creamos un contador en el que si se pasan de 40 cartas creadas, vamos a realizar otra función con [else], que nos ofrezca volver a empezar la partida.

### Problema 3:

Ahora, queremos que cuando pinchemos una carta, nos de su información. Vamos a seleccionar el array de misCartas. Para ello vamos a seleccionarlo con el selector (en este caso querySelectorAll).

Nos aprovecharemos de la variable contador, porque al ir a la par, podemos reutilizarla para saber cuál carta es la que hay en el array que hemos creado nuevo llamado misCartas. (Es decir, del 0 al 39. )

Usaremos [this] para que haga referenica al ámbito(objeto) en el que se encuentre, en este caso, la propia función (misCartas[contador]).

```js
function dameCarta() {
  // Problema 2.
  //¿Hasta cuando voy a pedir cartas?
  if (contador < 40) {
    // Problema 1.
    // ¿Cómo pinto los elementos?
    esta = document.createElement("p");
    esta.innerHTML = mostrarCarta();
    document.getElementById("carta").appendChild(esta);
    document.getElementByID("resultado").innerHTML = mostrarCarta();

    //Problema 3
    //¿Cómo veo que carta es pinchándola?

    misCartas = document.querySelectorAll("div#carta p");
    misCartar[contador].onclick = function () {
      alert("has hecho click en la carta " + this.innerHTML);
      contador++;
    };
  } else {
    alert("Se acabó");

    nuevaPartida = confirm("se acabo, ¿otra partida?")
    if (nuevaPartida){
        baraja[0] = crearArray();
        baraja[1] = crearArray();
        baraja[2] = crearArray();
        baraja[3] = crearArray();
        contador = 0;
        resultado="usada";
        document.getElementById("carta).innerHTML  ="";
    }
    else ("pues ya estaría, cierra la pestaña")
  }
}
```

## LA PARTE DE HTML

Ahora toca ver, cómo se mostrarían, para ello vamos a hacer lo siguiente en el body del HTML.

```html
<body>
  <button onclick="dameCarta()">dame carta socio</button>
  <div id="cartas"></div>
</body>
```

En el caso de que no queramos hacer "onclick" dentro del html del botón, podríamos usar una de las siguientes formas:

```js
window.onload = function () {
  document.getElementById("boton").onlick;

  //onclick es para el elemento directamente.
  //event listener es un metodo

  boton.addEventListener("click", dameCarta);
};
```

```html
<body>
  <button id="boton">dame carta socio</button>
  <div id="cartas"></div>
</body>
```

En ambos casos, tenemos que esperar a que todo el documento esté cargado, por ello, le damos window.onload y luego la función.

Después de eso, le añadimos el método en el que podemos realizar una función en base a hacer click en el input con el [ID = boton]

## PARTE DE CSS

Para que tenga aspecto de carta vamos a hacer lo siguiente:

```css
#carta{display:flex;justify-content:flex-start;flex-wrap:wrap;gap:20px}

p{width:80px;height;120px;border:1px solid #000}
```

Seleccionamos el id de carta, le damos la propiedad flex que colocará de forma automática los nuevos elementos que salgan, además le añadimos propiedades como dónde empieza y cómo se reparten.
