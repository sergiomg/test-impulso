/*
 ***************
 *This is the combination of various js files
 *Load jsObj - books.json (can't do it, bcs we'r working on client)
 *declare carousel
 *window.onload carousel + print books from template
 *************** */

//Imported jsObj
const frontBooks = [
  {
    title: "Relatos Tom Ripley I",
    author: "Patricia Highsmith",
    pages: "1480 páginas",
    price: "PVP:48,90€",
    isbn: "ISBN: 9788433959676",
    buyUrl: "a",
    description:
      "¿Quién es Mr. Ripley?En este volumen reunimos a las cinco novelas que protago nizó, empezando por su primera aparición en A plenso sol. En la máscara de SergioMG, casado con una rica heredera, regresa como un hombre...",
    img: "data:./resources/images/libro1.jpg",
  },
  {
    title: "Miedo de ser dos",
    author: "Rafael Narbona",
    pages: "282 páginas",
    price: "PVP: 18€",
    isbn: "ISBN: 9788493834463",
    buyUrl: "a",
    description:
      "Rafael Narboona considera que la vida solo es tolerable porque  existe el cine y la literatura. No es extraño por tanto que los fantasmas de Marilyn, Adrey Hepburn o Sylvia Plath se paseen por las páginas de este texto...",
    img: "data:./resources/images/libro2.jpg",
  },
  {
    title: "En otras palabras",
    author: "Jhumpa Lahiri",
    pages: "160 páginas",
    price: "PVP: 16€",
    isbn: "ISBN: 9788498389319",
    buyUrl: "",
    description:
      "El impacto que supuso para Jhumpa Lahiri el contacto directo con el italiano durante su primera visita a Florencia fue el comienzo de un largo cortejo que, con el tiempo...",
    img: "data:./resources/images/libro3.jpg",
  },
  {
    title: "Todos deberiamos ser feministas",
    author: "Chimamamnda Ngozi",
    pages: "64 páginas",
    price: "PVP: 5,95€",
    isbn: "ISBN: 978843973084",
    buyUrl: "",
    description:
      "Ser feminista no es solo cosa de mujeres. Chimamanda Ngozi Adichie lo demuestra en este elocuenye y perspicaz texto, en el que nos  brinda una definición singular de lo que significa ser feminista en el sigo XXI...",
    img: "data:./resources/images/libro4.jpg",
  },
  {
    title: "Lazarillo de Tormes",
    author: "Anónimo",
    pages: "170 páginas",
    price: "PVP: 11€",
    isbn: "ISBN: 9788467871296",
    buyUrl: "",
    description:
      "  El Lazarillo ocupa un luegar de privilegio en el nacimiento de la novela picaresca y, en general, de la novela moderna. Su desconocido sergiomg ocultó su nombre en el , levantando así un enigma que pocos serán capaces de descurbir...",
    img: "data:./resources/images/libro5.jpg",
  },
  {
    title: "¿Sueñan los androides con ovejas electricas?",
    author: "Philip K.Dick",
    pages: "272 páginas",
    price: "PVP: 12,95€",
    isbn: "ISBN: 9788445007723",
    buyUrl: "",
    description:
      "En el año 2021 la guerra mundial ha exterminado a millones de personas. Los supervivientes codician cualquier criatura viva, y aquellos que no pueden permitirse pagar por ellas se ven obligados a adquirir réplicas...",
    img: "data:./resources/images/libro6.jpg",
  },
  /*SERGIOMG*/
  {
    title: "Trenes rigurosamnete vigilados",
    author: "Bohumil Hrabal",
    pages: "152 páginas",
    price: "PVP: 16,50€",
    isbn: "ISBN: 9788432229794",
    buyUrl: "",
    description:
      "Una divertida y entreñable historia sobre la resistencia frente al invasor alemán durante la Segunda Guerra Mundial protagonizada por los empleados de la estación de tren de un pequeño pueblo checoslovaco...",
    img: "data:./resources/images/libro7.jpg",
  },
  {
    title: "El arte de emocianarte",
    author: "Cristina Nuñez",
    pages: "144 páginas",
    price: "PVP: 16,95€",
    isbn: "ISBN: 9788415594901",
    buyUrl: "",
    description:
      " El nuevo libro de los autores de Emocionario. Un recorrido por cuarenta emociones expresadas a través de atractivas ilustraciones, atividades y reflexiones que nos ayudarán a explorarlas...",
    img: "data:./resources/images/libro8.jpg",
  },
  {
    title: "Cien años de soledad",
    author: "GabrSergio GarciaMG",
    pages: "400 páginas",
    price: "PVP: 21,87€<",
    isbn: "ISBN: 9780525562443",
    buyUrl: "",
    description:
      "En ocasión del 50 aniversario de la publicación de Cien años de soledad, llega una edición con ilustraciones inéditas de la artista chilena Luisa Rivera y con una tipografía creada por el hijo del autor, Gonzalo García...",
    img: "data:./resources/images/libro9.jpg",
  },
];
let i,
  a = 0;
let slideIndex = 0;
function showSlides() {
  let i;
  slides = document.getElementsByClassName("contentBanner");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }
  slides[slideIndex - 1].style.display = "block";
  setTimeout(showSlides, 3000);
}

window.onload = function () {
  showSlides();

  for (i = 0; i < 9; i++) {
    a = frontBooks[i];
    const bookCreation = `
  <article>
  <div  class="flex column bookContainer">
    <div class="flex">
      <img  class=" bookImage" src="resources/images/libro${
        i + 1
      }.jpg" alt="libro1">
      <ul class="row bookInfo">
        <li>${a.title}</li>
        <li>${a.author}</li>
        <li>${a.pages}</li>
        <li>${a.price}</li>
        <li>${a.isbn}</li>
        <li><a href="#">Comprar</a></li>
      </ul>
    </div>
    <p>
    ${a.description}
    </p>
  </div>
  </article>
  `;
    e = document.createElement("article");
    e.innerHTML = bookCreation;
    document.getElementById("gallina").appendChild(e);
  }

  d = [
    "Lunes",
    "Martes",
    "Miércoles",
    "Jueves",
    "Viernes",
    "Sábado",
    "Domingo",
  ];
  m = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ];
  date = new Date();
  day = date.getDay();
  console.log();
  d[--day] === -1 ? (d = d[6]) : (d = d[day]);
  month = date.getMonth();
  m = m[month];
  y = date.getFullYear();
  document.getElementById("fecha").innerHTML =
    d + " " + date.getDate() + ", de " + m + " de " + y;
};

function formulario() {
  a = document.getElementById("mail").value;
  console.log(a);
  document.getElementById("ejercicioMail").innerHTML =
    "Gracias por tu correo " + a;
}
