https://gitlab.com/sergiomg/test-impulso/-/tree/master/EXAMEN%20MOD%201

El examen práctico del día 15 constará de dos partes:

PRIMERA PARTE. En casa: maquetación de una página web siguiendo el diseño adjunto y con estas características:

La maquetación debe utilizar para el contenido principal el modelo flexbox
La tipografía usada es Montserrat y Open Sans, que pueden descargarse de Google Fonts.
El menú principal (el superior) lleva un efecto rollover
El menú secundario (parte izquierda, géneros) debe tener un segundo nivel colapsable
SEGUNDA PARTE. En el aula el día del examen: añadir funcionalidades Javascript a la web previamente maquetada:

Añadir dos pequeñas funcionalidades Javascript a elegir entre tres
Añadir una funcionalidad Javascript adicional más compleja
Se adjunta el diseño en PNG y en PSD.

=====

RESPUESTAS EXAMEN:

2.  Lenguaje HTML obligatorio cerrar etiquetas.
    b. = XHTML

3.  <dl> sirve para..
    b. listas

4.  Titulo de la tabla
    c. caption

5.  etiqueta para Agrupar formularios en mismo nombre?
    b. fieldset

6.  evitar que un formulario se envie al pulsar boton
    d. #

7.  Incluir css fichero html
    d. regla @media-query

8.  css a todos los elementos? selector universal.
    b. \*

9.  ID vs CLASS - id = rojo, class= azul
    a. rojo

10. pseudoclase para decir que ah recibido la acción del enlace

11. combinador css elemento parrafo cuando no es adyacente
    c p ~ p

12. Para que sirve box-sizing definido de un elemento bloque
    a. border-box

13. que caracteristicas debe cumplir un contendor (que no sea <body> de un elemento si quiero posicionar este elemento de forma absoluta?)
    a. tener posición absoluta o relativa

14. justify-content
    a. alinear el eje principal definidio como display-flex

15. Como hacer que una funcion sea variable global?
    c no declarandolo

16. siguientes variables es una constante?
    c. var VALOR=5 (porque está en mayúsuclas)

17. resultado de operación aritmetica +"1"+4
    c. 5

18. valor lógico de la siguiente expresión: 1=="1"?
    b. true  
    (Porque son 2 signos, es valor, no tipo, para eso sería 3 =)

19. que método array incluir algo al final?
    b. push(elemento)

20. Cómo seleccionar todos los elementos clase miClase?
    d. querySelectorAll("miClase")
